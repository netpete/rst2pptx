
from setuptools import setup

setup(
    scripts=['Rst2pptx.py'],
    install_requires=['docutils', 'python-pptx'],
    )
